#include <MacTypes.h>
#include <TargetConditionals.h>

#include <CoreFoundation/CoreFoundation.h>
#include <CoreGraphics/CoreGraphics.h>

#define COCOA 1
#define TARGET_MAC_x86 1
#define EResolver(entryName) HCallResolver(entryName)

//void* HCallResolver(const char *entryName);

#define FLAT_C_PLUGIN_HEADERS 1


// Item Registration API
#define USE_SDK_REGISTERCONTROL
//#define USE_SDK_REGISTERMETHOD
//#define USE_SDK_REGISTERINTERFACE
//#define USE_SDK_REGISTERCLASSEXTENSION
//#define USE_SDK_REALRegisterModule
#define USE_SDK_REGISTERCLASS
#define USE_SDK_REALCreateArray
// Object Access
#define USE_SDK_GETCLASSDATA
//#define USE_SDK_NEWINSTANCE
//#define USE_SDK_REALLockObject
//#define USE_SDK_REALUnlockObject
#define USE_SDK_NEWINSTANCECLASSREF
//#define USE_SDK_INTERFACEROUTINE
#define USE_SDK_GETEVENTINSTANCE
#define USE_SDK_REALGetClassRef
#define USE_SDK_GET_INTEGERPROPERTY
#define USE_SDK_REALSetArrayValueInt32
#define USE_SDK_REALGetPropValueObject
#define USE_SDK_REALGetPropValueInt32
#define USE_SDK_REALSetPropValueInt32
#define USE_REALLoadObjectMethod
#define USE_SDK_REALUnlockObject
#define USE_SDK_REALGetPropValueUINT8
#define USE_SDK_REALInsertArrayValueString
#define USE_SDK_REALObjectIsA
// String Access
//#define USE_SDK_REALBuildString
#define USE_SDK_REALLockString
//#define USE_SDK_REALUnlockString
#define USE_SDK_REALSetStringEncoding
//#define USE_SDK_REALGetStringEncoding
//#define USE_SDK_REALGetStringCFString
//#define USE_SDK_REALGetStringSystemStr
//#define USE_SDK_REALstringToOSType
//#define USE_SDK_REALStripAmpersands
//#define USE_SDK_REALGetFontEncoding
//#define USE_SDK_REALConvertString
//#define USE_SDK_REALGetWin32Charset


// Control API
// -------------------------------------------------------------
#define USE_SDK_GETCONTROLBOUNDS
#define USE_SDK_GETCONTROLDATA
//#define USE_SDK_SETCONTROLPOSITION
//#define USE_SDK_GETCONTROLPOSITION
#define USE_SDK_GETCONTROLENABLED
//#define USE_SDK_SETCONTROLENABLED
//#define USE_SDK_GETCONTROLVISIBLE
//#define USE_SDK_SETCONTROLVISIBLE
//#define USE_SDK_GETTABPANELVISIBLE
#define USE_SDK_REALGETCONTROLGRAPHICS
#define USE_SDK_INVALIDATE_CONTROL
//#define USE_SDK_INVALIDATE_CONTROLRECT
//#define USE_SDK_SETCONTROLFOCUS
#define USE_SDK_GETCONTROLFOCUS
//#define USE_SDK_GETCONTROLHANDLE
//#define USE_SDK_GETCONTROLPARENT
//#define USE_SDK_GETCONTROLNAME
//#define USE_SDK_REALDefaultControlFont
//#define USE_SDK_REALDefaultControlCaption
//#define USE_SDK_REALDefaultControlFontSize
#define USE_SDK_REALGetControlWindow
//#define USE_SDK_REALSetSpecialBackground
//#define USE_SDK_REALGraphicsDrawOffscreenMacControl
//#define USE_SDK_REALGetControlHWND

// Picture API
#define USE_SDK_NEWPICTURE
//#define USE_SDK_PICTUREDESCRIPTIONLOCKS
//#define USE_SDK_DRAWPICTUREPRIMITIVE
#define USE_SDK_GETPICTUREGRAPHICS
//#define USE_SDK_BUILDPICTUREFROMBUFFER
#define USE_SDK_GETPICTUREMASK
//#define USE_SDK_REALBuildPictureFromGWorld
//#define USE_SDK_REALBuildPictureFromPictureDescription
//#define USE_SDK_REALBuildPictureFromPicHandle
//#define USE_SDK_REALBuildPictureFromDIB



// Graphics API
#define USE_SDK_REALSelectGraphics
//#define USE_SDK_GRAPHICS_DRAWLINE
//#define USE_SDK_REALGraphicsDrawString
//#define USE_SDK_REALGetGraphicsOrigin
//#define USE_SDK_REALSetGraphicsOrigin
//#define USE_SDK_REALGetGraphicsFontStyle
//#define USE_SDK_REALSetGraphicsStyle
//#define USE_SDK_REALGraphicsStringWidth
//#define USE_SDK_REALGraphicsStringHeight
//#define USE_SDK_REALGraphicsTextHeight
//#define USE_SDK_REALGraphicsTextAscent
//#define USE_SDK_REALGraphicsDC

// Environment API
#define USE_SDK_INRUNTIME
//#define USE_SDK_REALInDebugMode
#define USE_SDK_REALGetRBVersion


// Window API
#define USE_SDK_REFRESHWINDOW 
#define USE_SDK_ISHIVIEWWINDOW
#define USE_SDK_REALGetWindowHandle

// FolderItem API
//#define USE_SDK_PATHFROMFOLDERITEM
//#define USE_SDK_FOLDERITEMFROMPATH
//#define USE_SDK_FOLDERITEMFROMFSSPEC
//#define USE_SDK_FSSPECFROMFOLDERITEM
//#define USE_SDK_FSREFFROMFOLDERITEM
//#define USE_SDK_FOLDERITEMFROMPARENTFSREF


//#define USE_SDK_InterpretConstantValue
//#define USE_SDK_REALRaiseException

//#include "RBCocoaHeaders.h"
//#include "REALplugin.h"

#include "rb_plugin.h"
#include "REALplugin.h"

#ifdef NewPtr
    #undef NewPtr
#endif

#ifdef DisposePtr
    #undef DisposePtr
#endif


#define NewPtr(size)         malloc(size) //new char[size]
#define DisposePtr(ptr)        free(ptr)    // delete []ptr
#define ResizePtr(ptr,size) realloc(ptr,size)
#define Ptr                    char*









