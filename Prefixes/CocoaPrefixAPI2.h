#define COCOA 1
#define API2 1

#define FLAT_C_PLUGIN_HEADERS 1
#define ALLOW_CARBON_IN_COCOA 0

#define TARGET_CARBON ALLOW_CARBON_IN_COCOA

#define EResolver(entryName) HCallResolver(entryName)


#define FLAT_C_PLUGIN_HEADERS 1


#ifdef __OBJC__
    #import <Cocoa/Cocoa.h>
#else
	
#endif

#if ALLOW_CARBON_IN_COCOA
	#import <Carbon/Carbon.h>
#endif

#import <CoreServices/CoreServices.h>

#include "rb_plugin.h"
#include "REALplugin.h"
