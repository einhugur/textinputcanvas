
#ifndef __EinhugurLinuxPrefix__
#define __EinhugurLinuxPrefix__

#define UNIX_ANSI 1
#define X_WINDOW 1
#define XOJO_SDK 1
#define FLAT_C_PLUGIN_HEADERS 1

//#define nullptr NULL

#define EResolver(entryName) HCallResolver(entryName)

#ifndef NULL
	#define NULL 0
#endif

#include <stdint.h>

#ifndef assert
	#define assert(cond) if (!(cond));
	#define debugAssert(cond) assert(cond)
	#define assertMsg(cond, msg) assert(cond)
#endif

#define XOJO_GTK3 1

//#include "macTypesForAnsi.h"


namespace QT {
	typedef void *MovieController;
	typedef void *Movie;
};

#endif	// __EinhugurLinuxPrefix__

