# makefile
#
#	This is the Linux/x86 makefile for the TextInputCanvas plugin.
#
# (c) 2013 Xojo, Inc. -- All Rights Reserved

MACHINE := $(shell uname -m)

ifeq ($(MACHINE), aarch64)
	CC = clang++
else
	CC = clang++-3.6 
endif

MACHINE := $(shell uname -m)
BASEDIR = ..
# Compile for Xojo
# -----------------------------------------------------------------
XOJOSDK = $(BASEDIR)/SDK/Xojo\ SDK\ 2
SDKGLUECODEDIR = $(XOJOSDK)/Glue\ Code
SDKINCLUDEDIR = $(XOJOSDK)/Includes
# -----------------------------------------------------------------
SOURCEDIR = src

INCLUDE =	-iquote$(SDKINCLUDEDIR) \
		-iquote$(SOURCEDIR) \
		-iquote./

ifeq ($(MACHINE), x86_64)
	DESTINATION = Full\ x64
	DESTINATIONPLAIN = Full x64
else
	ifeq ($(MACHINE), armv7l)
		DESTINATION = Full\ ARM
		DESTINATIONPLAIN = Full ARM
	else
		ifeq ($(MACHINE), aarch64)
			DESTINATION = Full\ ARM64
			DESTINATIONPLAIN = Full ARM64
		else
			DESTINATION = Full
			DESTINATIONPLAIN = Full
		endif
	endif
endif

		

GTKINCLUDES = `pkg-config --cflags gtk+-3.0`
GTKLIBS = `pkg-config --libs gtk+-3.0`

LIBS = $(GTKLIBS)

ifeq ($(MACHINE), x86_64)
CFLAGS = -fPIC  -std=c++11 -O2 -D__INTEL__ -D__GCC__ -DWIDGET_GTK=1 $(INCLUDE) $(GTKINCLUDES)
else
	ifeq ($(MACHINE), armv7l)
		CFLAGS = -std=c++11 -O2 -D__INTEL__ -D__GCC__ -DWIDGET_GTK=1 $(INCLUDE) $(GTKINCLUDES)
	else
		ifeq ($(MACHINE), aarch64)
			CFLAGS = -fPIC  -std=c++11 -O2 -D__INTEL__ -D__GCC__ -DWIDGET_GTK=1 $(INCLUDE) $(GTKINCLUDES)
		else
			CFLAGS = -std=c++11 -O2 -D__INTEL__ -D__GCC__ -DWIDGET_GTK=1 $(INCLUDE) $(GTKINCLUDES)
		endif
	endif
endif


PREFIXHEADER = Prefixes/LinuxPrefix.h
PREFIXHEADERAPI2 = Prefixes/LinuxPrefixAPI2.h

PRODUCT = TextInputCanvas.so
BUILDOUTPUTDIR = BuildObjects
BUILDOUTPUTDIRAPI2 = BuildObjectsAPI2

OBJS =	$(BUILDOUTPUTDIR)/main.o \
	$(BUILDOUTPUTDIR)/PluginMain.o \
	$(BUILDOUTPUTDIR)/TextInputCanvas.o \
	$(BUILDOUTPUTDIR)/TextInputGTK.o \
	$(BUILDOUTPUTDIR)/TextRange.o \
	$(BUILDOUTPUTDIR)/XojoAPISwitch.o \
	$(BUILDOUTPUTDIR)/UnicodeHelper.o

	

OBJSAPI2 =	$(BUILDOUTPUTDIRAPI2)/main.o \
	$(BUILDOUTPUTDIRAPI2)/PluginMain.o \
	$(BUILDOUTPUTDIRAPI2)/TextInputCanvas.o \
	$(BUILDOUTPUTDIRAPI2)/TextInputGTK.o \
	$(BUILDOUTPUTDIRAPI2)/TextRange.o \
	$(BUILDOUTPUTDIRAPI2)/XojoAPISwitch.o \
	$(BUILDOUTPUTDIRAPI2)/UnicodeHelper.o

all: createbuilddir $(OBJS) createbuilddirapi2 $(OBJSAPI2)
	mkdir -p Linux\ Build/$(DESTINATION)

	$(CC) $(CFLAGS) -shared -o Linux\ Build/$(DESTINATION)/$(PRODUCT) $(OBJS) $(LIBS)

	mkdir -p Linux\ Build/$(DESTINATION)\ API2

	$(CC) $(CFLAGS) -shared -o Linux\ Build/$(DESTINATION)\ API2/$(PRODUCT) $(OBJSAPI2) $(LIBS)

push:
ifeq ($(MACHINE), aarch64)
	../buildagent/buildagent push TextInputCanvas "Linux Build/$(DESTINATIONPLAIN)" "$(shell pwd)/Linux Build/$(DESTINATIONPLAIN)/"
	../buildagent/buildagent push TextInputCanvas "Linux Build/$(DESTINATIONPLAIN) API2" "$(shell pwd)/Linux Build/$(DESTINATIONPLAIN) API2/"
else
	../buildagent/buildagent TextInputCanvas "Linux Build/$(DESTINATIONPLAIN)" "$(shell pwd)/Linux Build/$(DESTINATIONPLAIN)/"
	../buildagent/buildagent TextInputCanvas "Linux Build/$(DESTINATIONPLAIN) API2" "$(shell pwd)/Linux Build/$(DESTINATIONPLAIN) API2/"
endif

createbuilddir:
	mkdir -p $(BUILDOUTPUTDIR)

$(BUILDOUTPUTDIR)/main.o: $(SOURCEDIR)/main.cpp
	$(CC) $(CFLAGS) -include $(PREFIXHEADER) "$<" -c -o $@

$(BUILDOUTPUTDIR)/PluginMain.o: $(SDKGLUECODEDIR)/PluginMain.cpp
	$(CC) $(CFLAGS) -include $(PREFIXHEADER) "$<" -c -o $@

$(BUILDOUTPUTDIR)/TextInputCanvas.o: $(SOURCEDIR)/TextInputCanvas.cpp
	$(CC) $(CFLAGS) -include $(PREFIXHEADER) "$<" -c -o $@

$(BUILDOUTPUTDIR)/TextInputGTK.o: $(SOURCEDIR)/TextInputGTK.cpp
	$(CC) $(CFLAGS) -include $(PREFIXHEADER) "$<" -c -o $@

$(BUILDOUTPUTDIR)/TextRange.o: $(SOURCEDIR)/TextRange.cpp
	$(CC) $(CFLAGS) -include $(PREFIXHEADER) "$<" -c -o $@

$(BUILDOUTPUTDIR)/XojoAPISwitch.o: $(SOURCEDIR)/XojoAPISwitch.cpp
	$(CC) $(CFLAGS) -include $(PREFIXHEADER) "$<" -c -o $@

$(BUILDOUTPUTDIR)/UnicodeHelper.o: $(SOURCEDIR)/UnicodeHelper.cpp
	$(CC) $(CFLAGS) -include $(PREFIXHEADER) "$<" -c -o $@

createbuilddirapi2:
	mkdir -p $(BUILDOUTPUTDIRAPI2)

$(BUILDOUTPUTDIRAPI2)/main.o: $(SOURCEDIR)/main.cpp
	$(CC) $(CFLAGS) -include $(PREFIXHEADERAPI2) "$<" -c -o $@

$(BUILDOUTPUTDIRAPI2)/PluginMain.o: $(SDKGLUECODEDIR)/PluginMain.cpp
	$(CC) $(CFLAGS) -include $(PREFIXHEADERAPI2) "$<" -c -o $@

$(BUILDOUTPUTDIRAPI2)/TextInputCanvas.o: $(SOURCEDIR)/TextInputCanvas.cpp
	$(CC) $(CFLAGS) -include $(PREFIXHEADERAPI2) "$<" -c -o $@

$(BUILDOUTPUTDIRAPI2)/TextInputGTK.o: $(SOURCEDIR)/TextInputGTK.cpp
	$(CC) $(CFLAGS) -include $(PREFIXHEADERAPI2) "$<" -c -o $@

$(BUILDOUTPUTDIRAPI2)/TextRange.o: $(SOURCEDIR)/TextRange.cpp
	$(CC) $(CFLAGS) -include $(PREFIXHEADERAPI2) "$<" -c -o $@

$(BUILDOUTPUTDIRAPI2)/XojoAPISwitch.o: $(SOURCEDIR)/XojoAPISwitch.cpp
	$(CC) $(CFLAGS) -include $(PREFIXHEADERAPI2) "$<" -c -o $@

$(BUILDOUTPUTDIRAPI2)/UnicodeHelper.o: $(SOURCEDIR)/UnicodeHelper.cpp
	$(CC) $(CFLAGS) -include $(PREFIXHEADERAPI2) "$<" -c -o $@


clean:
	rm -rf $(BUILDOUTPUTDIR)
	rm -rf $(BUILDOUTPUTDIRAPI2)
	rm -f Linux\ Build/$(DESTINATION)/$(PRODUCT)
	rm -f Linux\ Build/$(DESTINATION)\ API2/$(PRODUCT)
