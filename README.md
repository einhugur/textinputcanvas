**TextInputCanvas**
=======

TextInputCanvas is a plugin for the [Xojo programming environment](http://xojo.com) that allows developers to implement custom text input controls with international input support.

Our TextInputCanvas is fork of the TextInputCanvas from Xojo Inc at: https://github.com/xojo/TextInputCanvas

We have progressed their version and provide builds for everyone for all platforms.


**Note:** We cannot help you with setting up build tool chain or provide you with any custom tools we use in our tool chain.


**Download built plugin from:**

https://einhugur.com/Downloads/Plugs/TextInputCanvas.zip

**This plugin is for example needed for the excellent FormattedText control, which can be found here:**

https://github.com/atomiccity/FormattedTextControl

**It's also needed for Garry Pettets XUI Code Editor control, which can be found here:** 

https://xui.software/products/xui-desktop/code-editor

**License**
-------

TextInputCanvas is released under the zlib license by Xojo Inc, and changes Licensed by Einhugur. See the LICENSE file for additional details.
