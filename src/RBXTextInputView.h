//
//  RBXTextInputView.h
//
//  Original parts of this code (c) 2013 Xojo, Inc. -- All Rights Reserved
//  New parts of this code (c) Einhugur Software and contributors. -- All Rights Reserved

#ifndef ___RBXTextInputView___
#define ___RBXTextInputView___

#import <Cocoa/Cocoa.h>
#import "rb_plugin.h"
#import "rb_plugin_cocoa.h"

@interface XOJTextInputView : NSView <NSTextInputClient> {
	REALcontrolInstance control;
    bool allowFocusRing;
}

- (id)initWithControlInstance:(REALcontrolInstance)controlInstance;
- (void)cleanup;
- (void)setAllowFocusRing:(bool)value;

@end

#endif
