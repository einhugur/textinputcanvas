#include "XojoAPISwitch.h"

void (*GraphicsFillRectangle)  (REALgraphics, double, double, double, double);
void (*GraphicsDrawRectangle)  (REALgraphics, double, double, double, double);

void InitApiSwitch()
{
    GraphicsFillRectangle = NULL;
    GraphicsDrawRectangle = NULL;
}

#ifdef API2
    void LoadGraphicsFunctions(REALgraphics g, double xojoVersion)
    {
        GraphicsFillRectangle = (void(*)(REALgraphics, double, double, double, double)) REALLoadObjectMethod((REALobject)g, "FillRectangle(x as Double,y as Double,width as Double,height as Double)");
        GraphicsDrawRectangle = (void(*)(REALgraphics, double, double, double, double)) REALLoadObjectMethod((REALobject)g, "DrawRectangle(x as Double,y as Double,width as Double,height as Double)");
    }

    /**
     * Converts a Rect into a Xojo.Rect when using API object.
     */
    REALobject CreateRectObject( REALclassRef rectClassRef, const Rect* rect )
    {
        typedef void (* ConstructorProc)( REALobject, double, double, double, double );
        static ConstructorProc RectConstructor = NULL;
        
        REALobject rectObject = REALnewInstance( rectClassRef );
        
        if(RectConstructor == NULL )
        {
            RectConstructor = (ConstructorProc)REALLoadObjectMethod( rectObject, "Constructor( x As Double, y As Double, w As Double, h As Double)" );
        }
        
        RectConstructor( rectObject, rect->left, rect->top, rect->right - rect->left, rect->bottom - rect->top );
        
        return rectObject;
    }
#else
    /**
     * Converts a Rect into a REALbasic.Rect when using API object.
     */
    REALobject CreateRectObject( REALclassRef rectClassRef, const Rect* rect )
    {
        typedef void (* ConstructorProc)( REALobject, RBInteger, RBInteger, RBInteger, RBInteger );
        static ConstructorProc RectConstructor = NULL;
        
        
        REALobject rectObject = REALnewInstance( rectClassRef );
        
        if(RectConstructor == NULL )
        {
            RectConstructor = (ConstructorProc)REALLoadObjectMethod( rectObject, "Constructor( x As Integer, y As Integer, w As Integer, h As Integer)" );
        }
        
        RectConstructor( rectObject, rect->left, rect->top, rect->right - rect->left, rect->bottom - rect->top );
        
        return rectObject;
    }

    // Xojo older than 2018.02 needs the legacy handlers. Legacy handlers are not as optimized.
    static void LegacyFillRect(REALgraphics g, double x, double y, double width, double height)
    {
        static void (*FillRect_fp)    (REALgraphics, RBInteger, RBInteger, RBInteger, RBInteger) = NULL;
        
        if(FillRect_fp == NULL)
        {
            FillRect_fp = (void(*)(REALgraphics, RBInteger, RBInteger, RBInteger, RBInteger)) REALLoadObjectMethod((REALobject)g, "FillRect(x as Integer,y as Integer,width as Integer,height as Integer)");
        }
        
        FillRect_fp(g, (RBInteger)x, (RBInteger)y, (RBInteger)width, (RBInteger)height);
    }

    static void LegacyDrawRect(REALgraphics g, double x, double y, double width, double height)
    {
        static void (*DrawRect_fp)    (REALgraphics, RBInteger, RBInteger, RBInteger, RBInteger) = NULL;
        
        if(DrawRect_fp == NULL)
        {
            DrawRect_fp = (void(*)(REALgraphics, RBInteger, RBInteger, RBInteger, RBInteger)) REALLoadObjectMethod((REALobject)g, "DrawRect(x as Integer,y as Integer,width as Integer,height as Integer)");
        }
        
        DrawRect_fp(g, (RBInteger)x, (RBInteger)y, (RBInteger)width, (RBInteger)height);
    }

    void LoadGraphicsFunctions(REALgraphics g, double xojoVersion)
    {
        if (xojoVersion >= 2018.02)
        {
            GraphicsFillRectangle = (void(*)(REALgraphics, double, double, double, double)) REALLoadObjectMethod((REALobject)g, "FillRect(x as Double,y as Double,width as Double,height as Double)");
            GraphicsDrawRectangle = (void(*)(REALgraphics, double, double, double, double)) REALLoadObjectMethod((REALobject)g, "DrawRect(x as Double,y as Double,width as Double,height as Double)");
        }
        else
        {
            GraphicsFillRectangle = &LegacyFillRect;
            GraphicsDrawRectangle = &LegacyDrawRect;
        }
    }
#endif
