//
//  TextRange.cpp
//
//  (c) 2013 Xojo, Inc. -- All Rights Reserved
//

#include "TextRange.h"

static void TextRangeConstructor( REALobject range );
static void TextRangeConstructorWRange( REALobject range, RBInteger location, RBInteger length );
static RBInteger TextRangeGetEnd( REALobject range, RBInteger param );
static void TextRangeSetEnd( REALobject range,  RBInteger param, RBInteger end );

struct TextRangeData
{
	RBInteger location;
	RBInteger length;
};

static REALproperty sTextRangeProperties[] = {
	{ nil, "Location", "Integer", 0, REALstandardGetter, REALstandardSetter, FieldOffset(TextRangeData, location) },
	{ nil, "Length", "Integer", 0, REALstandardGetter, REALstandardSetter, FieldOffset(TextRangeData, length) },
	{ nil, "EndLocation", "Integer", 0, (REALproc)TextRangeGetEnd, (REALproc)TextRangeSetEnd }
};

static REALmethodDefinition sTextRangeMethods[] = {
	{ (REALproc)TextRangeConstructor, nil, "Constructor()" },
	{ (REALproc)TextRangeConstructorWRange, nil, "Constructor( location As Integer, length As Integer )" }
};

static REALclassDefinition sTextRangeClass = {
	kCurrentREALControlVersion,
	"TextRange",
	nil, // superclass
	sizeof(TextRangeData),
	0, // forSystemUse
	nil, // constructor
	nil, // destructor
	sTextRangeProperties, sizeof(sTextRangeProperties) / sizeof(sTextRangeProperties[0]),
	sTextRangeMethods, sizeof(sTextRangeMethods) / sizeof(sTextRangeMethods[0])
};

static void TextRangeConstructor( REALobject range )
{
}

static void TextRangeConstructorWRange( REALobject range, RBInteger location, RBInteger length )
{
	TextRangeData *data = (TextRangeData *)REALGetClassData( range, &sTextRangeClass );
	data->location = location;
	data->length = length;
}

static RBInteger TextRangeGetEnd( REALobject range, RBInteger param )
{
	TextRangeData *data = (TextRangeData *)REALGetClassData( range, &sTextRangeClass );
	return data->location + data->length;
}

static void TextRangeSetEnd( REALobject range, RBInteger param, RBInteger end )
{
	TextRangeData *data = (TextRangeData *)REALGetClassData( range, &sTextRangeClass );
	data->length = end - data->location;
}

REALobject CreateTextRange( RBInteger begin, RBInteger length )
{
	static REALclassRef rangeClass = REALGetClassRef(sTextRangeClass.name);
	REALobject object = REALnewInstance(rangeClass);
	TextRangeData *data = (TextRangeData *)REALGetClassData( object, &sTextRangeClass );
	data->location = begin;
	data->length = length;
	return object;
}

void GetTextRangeInfo( REALobject range, RBInteger *begin, RBInteger *length )
{
	TextRangeData *data = (TextRangeData *)REALGetClassData( range, &sTextRangeClass );
	if (begin) *begin = data->location;
	if (length) *length = data->length;
}

void RegisterTextRangeClass()
{
	REALRegisterClass( &sTextRangeClass );
}
