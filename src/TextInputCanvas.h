//
//  TextInputCanvas.h
//
//  Original parts of this code (c) 2013 Xojo, Inc. -- All Rights Reserved
//  New parts of this code (c) Einhugur Software and contributors. -- All Rights Reserved
//

#ifndef IDEUtils_TextInputCanvas_h
#define IDEUtils_TextInputCanvas_h

#include "rb_plugin.h"

#if COCOA
    #include "RBXTextInputView.h"
#elif WINDOWS
    #include "TextInputWindows.h"
#elif WIDGET_GTK
    #include "TextInputGTK.h"
    #include "UnicodeHelper.h"
#endif

struct InputCanvasData
{
    #if COCOA
        XOJTextInputView *view;
    #elif WINDOWS
        TextInputWindows *view;
    #elif WIDGET_GTK
        TextInputGTK *view;
    #else
        void *padding;
    #endif
    
    RBBoolean AllowFocusRing;
    RBBoolean HasFocus;

    RBBoolean IsDesktopControl;
    
    RBInteger (*BaselineAtIndexEvent)(REALcontrolInstance, RBInteger index);
    RBInteger (*CharacterAtPointEvent)(REALcontrolInstance, RBInteger x, RBInteger y);
    RBBoolean (*DoCommandEvent)(REALcontrolInstance, REALstring command);
    REALstring(*FontNameAtLocationEvent)(REALcontrolInstance, RBInteger);
    float (*FontSizeAtLocationEvent)(REALcontrolInstance, RBInteger);
    void (*DiscardIncompleteTextEvent)(REALcontrolInstance);
    void  (*SetIncompleteTextEvent)(REALcontrolInstance, REALstring, REALobject, REALobject);
    void  (*InsertTextEvent)(REALcontrolInstance, REALstring, REALobject);
    RBBoolean(*IsEditableEvent)(REALcontrolInstance);
    RBBoolean(*KeyFallsThroughEvent)(REALcontrolInstance, REALstring);

    REALobject(*FireIncompleteTextRangeEvent)(REALcontrolInstance);
    
    void  (*PaintEvent)(REALcontrolInstance, REALgraphics g, REALarray areas);
    REALobject(*RectForRangeEvent)(REALcontrolInstance, REALobject*);
    REALobject(*SelectedRangeEvent)(REALcontrolInstance);
    REALstring(*TextForRangeEvent)(REALcontrolInstance, REALobject);

    RBInteger(*TextLengthEvent)(REALcontrolInstance);
    RBBoolean(*MouseDownEvent)(REALcontrolInstance, RBInteger, RBInteger);
    void (*MouseDragEvent)(REALcontrolInstance, RBInteger, RBInteger);
    void (*MouseUpEvent)(REALcontrolInstance, RBInteger, RBInteger);

    void (*GotFocusEvent)(REALcontrolInstance);
    void (*LostFocusEvent)(REALcontrolInstance);
    void (*EnableMenuItemsEvent)(REALcontrolInstance);
    void (*ScaleFactorChangedEvent)(REALcontrolInstance, double newFactor);

};

extern REALcontrol InputCanvasControl;


RBBoolean FireDoCommand( REALcontrolInstance control, const char *command );
REALstring FireFontNameAtLocation( REALcontrolInstance control, RBInteger location );
float FireFontSizeAtLocation( REALcontrolInstance control, RBInteger location );
REALobject FireIncompleteTextRange( REALcontrolInstance control );
void FireSetIncompleteText( REALcontrolInstance control, REALstring text,
	REALobject replacementRange, REALobject relativeSelection );
void FireInsertText( REALcontrolInstance control, REALstring text,
	REALobject range );
bool FireIsEditable( REALcontrolInstance control );
bool FireIsOverwriteMode( REALcontrolInstance control );
bool FireKeyFallsThrough( REALcontrolInstance control, REALstring key );
void FirePaint(REALcontrolInstance control, REALgraphics context, const Rect *rects, int rectCount);
REALobject FireRectForRange( REALcontrolInstance control, REALobject *range );
REALobject FireSelectedRange( REALcontrolInstance control );
REALstring FireTextForRange( REALcontrolInstance control, REALobject range );
RBInteger FireTextLength( REALcontrolInstance control );

void RegisterTextInputCanvasControl();

#endif
