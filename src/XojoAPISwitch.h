
#ifndef ___XOJO_API_SWITCH___
#define ___XOJO_API_SWITCH___

#include "rb_plugin.h"

#ifdef API2
    #define RectClassName "Xojo.Rect"
    #define DrawingColor "DrawingColor"
#else
    #define RectClassName "REALbasic.Rect"
    #define DrawingColor "ForeColor"
#endif

#define GraphicsFunctionsLoaded (GraphicsFillRectangle != NULL)

void InitApiSwitch();
void LoadGraphicsFunctions(REALgraphics g, double xojoVersion);

extern void (*GraphicsFillRectangle)  (REALgraphics, double, double, double, double);
extern void (*GraphicsDrawRectangle)  (REALgraphics, double, double, double, double);

REALobject CreateRectObject( REALclassRef rectClassRef, const Rect* rect );

#endif
