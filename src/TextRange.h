//
//  TextRange.h
//
//  (c) 2013 Xojo, Inc. -- All Rights Reserved
//

#ifndef IDEUtils_TextInputRange_h
#define IDEUtils_TextInputRange_h

#include "rb_plugin.h"

REALobject CreateTextRange( RBInteger begin, RBInteger length );
void GetTextRangeInfo( REALobject range, RBInteger *begin, RBInteger *length );
void RegisterTextRangeClass();

#endif
