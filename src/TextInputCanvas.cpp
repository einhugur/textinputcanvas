//
//  TextInputCanvas.cpp
//
//  (c) 2013 Xojo, Inc. -- All Rights Reserved
//

#include <string.h>
#include "TextInputCanvas.h"
#include "XojoAPISwitch.h"

#if COCOA
	#if !__OBJC__
		#error File must be compiled as Objective-C++
	#endif
	
	#import "RBXTextInputView.h"
#elif !TARGET_MACOS

enum
    {
        shiftKey                    = 1 << 9,
        optionKey                    = 1 << 11,
        controlKey                    = 1 << 12,
    };
	#if WINDOWS
		#include "TextInputWindows.h"
	#elif WIDGET_GTK
		#include "TextInputGTK.h"
		#include "UnicodeHelper.h"
	#endif

#endif // WINDOWS

extern double g_XojoVersion;
static REALclassRef RectClassRef;


static void InvalidateTextRects( REALcontrolInstance control );
static void LinuxSelectInputMethod( REALcontrolInstance control );

static RBBoolean KeyDownEvent( REALcontrolInstance control, int charCode, int keyCode, int modifiers );
#if WIDGET_GTK
static bool IsTextKey( int keycode, int modifiers );
	static RBBoolean UnfilteredKeyDownEvent( REALcontrolInstance control, int keyCode, int modifiers );
#endif
static void Initializer( REALcontrolInstance control );
static void DesktopInitializer(REALcontrolInstance control);
static void OpenEvent( REALcontrolInstance control );
static void CloseEvent( REALcontrolInstance control );
static void *HandleGetter( REALcontrolInstance control );
static RBBoolean MouseDown( REALcontrolInstance inst, int x, int y, int modifiers );
static void MouseDrag( REALcontrolInstance inst, int x, int y );
static void MouseUp( REALcontrolInstance inst, int x, int y );
static void GotFocus( REALcontrolInstance control );
static void LostFocus( REALcontrolInstance control );
static void EnableMenuItems( REALcontrolInstance control );
static void PreLoadEvents(REALcontrolInstance control);
static void ScaleFactorChanged(REALcontrolInstance control, double newScaleFactor);

static RBBoolean GetAllowFocusRing(REALcontrolInstance instance, RBInteger param)
{
    ControlData(InputCanvasControl, instance, InputCanvasData, me);
    
    return me->AllowFocusRing;
}

static void SetAllowFocusRing(REALcontrolInstance instance, RBInteger param, RBBoolean value)
{
    ControlData(InputCanvasControl, instance, InputCanvasData, me);
    
    #if TARGET_MACOS
        if(value != me->AllowFocusRing)
        {
            me->AllowFocusRing = value;
            
            if(me->view != NULL)
            {
                [me->view setAllowFocusRing:value ? YES : NO];
                
                if(me->HasFocus)
                {
                    me->view.needsDisplay = YES;
                }
            }
        }
    #else
        me->AllowFocusRing = value;
    #endif
}

static void FirePaintStub( REALcontrolInstance instance, REALgraphics g )
{
	FirePaint( instance, g, nullptr, 0 );
}

static REALmethodDefinition Methods[] =
{
	{ (REALproc)InvalidateTextRects, nullptr, "InvalidateTextRects()" },
	{ (REALproc)LinuxSelectInputMethod, nullptr, "LinuxSelectInputMethod()" },
	{ (REALproc)PreLoadEvents, nullptr, "ReloadEvents()"}
};

static REALevent Events[] =
{
	{ "BaselineAtIndex( index As Integer ) As Integer" },
	{ "CharacterAtPoint( x As Integer, y As Integer ) As Integer" },
	{ "DiscardIncompleteText()" },
	{ "DoCommand( command As String ) As Boolean" },
	{ "FontNameAtLocation( location As Integer ) As String" },
	{ "FontSizeAtLocation( location As Integer ) As Single" },
	{ "IncompleteTextRange() As TextRange" },
	{ "SetIncompleteText( text As String, replacementRange As TextRange, relativeSelection As TextRange )" },
	{ "InsertText( text As String, range As TextRange )" },
	{ "IsEditable() As Boolean" },
	{ "KeyFallsThrough( key As String ) As Boolean" },
    #ifdef API2
        { "Paint( g As Graphics, areas() As Xojo.Rect )" },
        { "RectForRange( ByRef range As TextRange ) As Xojo.Rect" },
    #else
        { "Paint( g As Graphics, areas() As REALbasic.Rect" },
        { "RectForRange( ByRef range As TextRange ) As REALbasic.Rect" },
    #endif
	{ "SelectedRange() As TextRange" },
	{ "TextForRange( range As TextRange ) As String " },
	{ "TextLength() As Integer" },
	{ "MouseDown(x As Integer, y As Integer) As Boolean" },
	{ "MouseDrag(x As Integer, y As Integer)" },
	{ "MouseUp(x As Integer, y As Integer)" },
	{ "GotFocus()" },
	{ "LostFocus()" },
	{ "EnableMenuItems()" },
    { "ScaleFactorChanged(newScaleFactor as Double)" }
};

static REALevent DesktopEvents[] =
{
	{ "BaselineAtIndex( index As Integer ) As Integer" },
	{ "CharacterAtPoint( x As Integer, y As Integer ) As Integer" },
	{ "DiscardIncompleteText()" },
	{ "DoCommand( command As String ) As Boolean" },
	{ "FontNameAtLocation( location As Integer ) As String" },
	{ "FontSizeAtLocation( location As Integer ) As Single" },
	{ "IncompleteTextRange() As TextRange" },
	{ "SetIncompleteText( text As String, replacementRange As TextRange, relativeSelection As TextRange )" },
	{ "InsertText( text As String, range As TextRange )" },
	{ "IsEditable() As Boolean" },
	{ "KeyFallsThrough( key As String ) As Boolean" },
	#ifdef API2
		{ "Paint( g As Graphics, areas() As Xojo.Rect )" },
		{ "RectForRange( ByRef range As TextRange ) As Xojo.Rect" },
	#else
		{ "Paint( g As Graphics, areas() As REALbasic.Rect" },
		{ "RectForRange( ByRef range As TextRange ) As REALbasic.Rect" },
	#endif
	{ "SelectedRange() As TextRange" },
	{ "TextForRange( range As TextRange ) As String " },
	{ "TextLength() As Integer" },
	{ "MouseDown(x As Integer, y As Integer) As Boolean" },
	{ "MouseDrag(x As Integer, y As Integer)" },
	{ "MouseUp(x As Integer, y As Integer)" },
	{ "FocusReceived()" },
	{ "FocusLost()" },
	{ "EnableMenuItems()" },
    { "ScaleFactorChanged(newScaleFactor as Double)" }
};

static REALconstant sInputCanvasConstants[] =
{
	#define CMD_CONST( x, y ) { #x " = \"" #y "\"" }
	
	// NSResponder: Selection movement and scrolling
	CMD_CONST(CmdMoveForward, moveForward:),
	CMD_CONST(CmdMoveRight, moveRight:),
	CMD_CONST(CmdMoveBackward, moveBackward:),
	CMD_CONST(CmdMoveLeft, moveLeft:),
	CMD_CONST(CmdMoveUp, moveUp:),
	CMD_CONST(CmdMoveDown, moveDown:),
	CMD_CONST(CmdMoveWordForward, moveWordForward:),
	CMD_CONST(CmdMoveWordBackward, moveWordBackward:),
	CMD_CONST(CmdMoveToBeginningOfLine, moveToBeginningOfLine:),
	CMD_CONST(CmdMoveToEndOfLine, moveToEndOfLine:),
	CMD_CONST(CmdMoveToBeginningOfParagraph, moveToBeginningOfParagraph:),
	CMD_CONST(CmdMoveToEndOfParagraph, moveToEndOfParagraph:),
	CMD_CONST(CmdMoveToEndOfDocument, moveToEndOfDocument:),
	CMD_CONST(CmdMoveToBeginningOfDocument, moveToBeginningOfDocument:),
	CMD_CONST(CmdPageDown, pageDown:),
	CMD_CONST(CmdPageUp, pageUp:),
	CMD_CONST(CmdCenterSelectionInVisibleArea, centerSelectionInVisibleArea:),
	
	// NSResponder: Selection movement and scrolling
	CMD_CONST(CmdMoveBackwardAndModifySelection, moveBackwardAndModifySelection:),
	CMD_CONST(CmdMoveForwardAndModifySelection, moveForwardAndModifySelection:),
	CMD_CONST(CmdMoveWordForwardAndModifySelection, moveWordForwardAndModifySelection:),
	CMD_CONST(CmdMoveWordBackwardAndModifySelection, moveWordBackwardAndModifySelection:),
	CMD_CONST(CmdMoveUpAndModifySelection, moveUpAndModifySelection:),
	CMD_CONST(CmdMoveDownAndModifySelection, moveDownAndModifySelection:),
	
	// NSResponder: Selection movement and scrolling
	CMD_CONST(CmdMoveToBeginningOfLineAndModifySelection, moveToBeginningOfLineAndModifySelection:),
	CMD_CONST(CmdMoveToEndOfLineAndModifySelection, moveToEndOfLineAndModifySelection:),
	CMD_CONST(CmdMoveToBeginningOfParagraphAndModifySelection, moveToBeginningOfParagraphAndModifySelection:),
	CMD_CONST(CmdMoveToEndOfParagraphAndModifySelection, moveToEndOfParagraphAndModifySelection:),
	CMD_CONST(CmdMoveToEndOfDocumentAndModifySelection, moveToEndOfDocumentAndModifySelection:),
	CMD_CONST(CmdMoveToBeginningOfDocumentAndModifySelection, moveToBeginningOfDocumentAndModifySelection:),
	CMD_CONST(CmdPageDownAndModifySelection, pageDownAndModifySelection:),
	CMD_CONST(CmdPageUpAndModifySelection, pageUpAndModifySelection:),
	CMD_CONST(CmdMoveParagraphForwardAndModifySelection, moveParagraphForwardAndModifySelection:),
	CMD_CONST(CmdMoveParagraphBackwardAndModifySelection, moveParagraphBackwardAndModifySelection:),

	// NSResponder: Selection movement and scrolling (added in 10.3)
	CMD_CONST(CmdMoveWordRight, moveWordRight:),
	CMD_CONST(CmdMoveWordLeft, moveWordLeft:),
	CMD_CONST(CmdMoveRightAndModifySelection, moveRightAndModifySelection:),
	CMD_CONST(CmdMoveLeftAndModifySelection, moveLeftAndModifySelection:),
	CMD_CONST(CmdMoveWordRightAndModifySelection, moveWordRightAndModifySelection:),
	CMD_CONST(CmdMoveWordLeftAndModifySelection, moveWordLeftAndModifySelection:),

	// NSResponder: Selection movement and scrolling (added in 10.6)
	CMD_CONST(CmdMoveToLeftEndOfLine, moveToLeftEndOfLine:),
	CMD_CONST(CmdMoveToRightEndOfLine, moveToRightEndOfLine:),
	CMD_CONST(CmdMoveToLeftEndOfLineAndModifySelection, moveToLeftEndOfLineAndModifySelection:),
	CMD_CONST(CmdMoveToRightEndOfLineAndModifySelection, moveToRightEndOfLineAndModifySelection:),
	
	// NSResponder: Selection movement and scrolling
	CMD_CONST(CmdScrollPageUp, scrollPageUp:),
	CMD_CONST(CmdScrollPageDown, scrollPageDown:),
	CMD_CONST(CmdScrollLineUp, scrollLineUp:),
	CMD_CONST(CmdScrollLineDown, scrollLineDown:),

	// NSResponder: Selection movement and scrolling
	CMD_CONST(CmdScrollToBeginningOfDocument, scrollToBeginningOfDocument:),
	CMD_CONST(CmdScrollToEndOfDocument, scrollToEndOfDocument:),

	// NSResponder: Graphical Element transposition
	CMD_CONST(CmdTranspose, transpose:),
	CMD_CONST(CmdTransposeWords, transposeWords:),

	// NSResponder: Selections
	CMD_CONST(CmdSelectAll, selectAll:),
	CMD_CONST(CmdSelectParagraph, selectParagraph:),
	CMD_CONST(CmdSelectLine, selectLine:),
	CMD_CONST(CmdSelectWord, selectWord:),
	
	// NSResponder: Insertions and Indentations
	CMD_CONST(CmdIndent, indent:),
	CMD_CONST(CmdInsertTab, insertTab:),
	CMD_CONST(CmdInsertBacktab, insertBacktab:),
	CMD_CONST(CmdInsertNewline, insertNewline:),
	CMD_CONST(CmdInsertParagraphSeparator, insertParagraphSeparator:),
	CMD_CONST(CmdInsertNewlineIgnoringFieldEditor, insertNewlineIgnoringFieldEditor:),
	CMD_CONST(CmdInsertTabIgnoringFieldEditor, insertTabIgnoringFieldEditor:),
	CMD_CONST(CmdInsertLineBreak, insertLineBreak:),
	CMD_CONST(CmdInsertContainerBreak, insertContainerBreak:),
	CMD_CONST(CmdInsertSingleQuoteIgnoringSubstitution, insertSingleQuoteIgnoringSubstitution:),
	CMD_CONST(CmdInsertDoubleQuoteIgnoringSubstitution, insertDoubleQuoteIgnoringSubstitution:),
	
	// NSResponder: Case changes
	CMD_CONST(CmdChangeCaseOfLetter, changeCaseOfLetter),
	CMD_CONST(CmdUppercaseWord, uppercaseWord:),
	CMD_CONST(CmdLowercaseWord, lowercaseWord:),
	CMD_CONST(CmdCapitalizeWord, capitalizeWord:),
	
	// NSResponder: Deletions
	CMD_CONST(CmdDeleteForward, deleteForward:),
	CMD_CONST(CmdDeleteBackward, deleteBackward:),
	CMD_CONST(CmdDeleteBackwardByDecomposingPreviousCharacter, deleteBackwardByDecomposingPreviousCharacter:),
	CMD_CONST(CmdDeleteWordForward, deleteWordForward:),
	CMD_CONST(CmdDeleteWordBackward, deleteWordBackward:),
	CMD_CONST(CmdDeleteToBeginningOfLine, deleteToBeginningOfLine:),
	CMD_CONST(CmdDeleteToEndOfLine, deleteToEndOfLine:),
	CMD_CONST(CmdDeleteToBeginningOfParagraph, deleteToBeginningOfParagraph:),
	CMD_CONST(CmdDeleteToEndOfParagraph, deleteToEndOfParagraph:),
	CMD_CONST(CmdYank, yank:),
	
	// NSResponder: Completion
	CMD_CONST(CmdComplete, complete:),
	
	// NSResponder: Mark/Point manipulation
	CMD_CONST(CmdSetMark, setMark:),
	CMD_CONST(CmdDeleteToMark, deleteToMark:),
	CMD_CONST(CmdSelectToMark, selectToMark:),
	CMD_CONST(CmdSwapWithMark, swapWithMark:),
	
	// NSResponder: Cancellation
	CMD_CONST(CmdCancel, cancel:),
	CMD_CONST(CmdCancelOperation, cancelOperation:),

	// NSResponder: Writing Direction
	CMD_CONST(CmdMakeBaseWritingDirectionNatural, makeBaseWritingDirectionNatural:),
	CMD_CONST(CmdMakeBaseWritingDirectionLeftToRight, makeBaseWritingDirectionLeftToRight:),
	CMD_CONST(CmdMakeBaseWritingDirectionRightToLeft, makeBaseWritingDirectionRightToLeft:),
	CMD_CONST(CmdMakeTextWritingDirectionNatural, makeTextWritingDirectionNatural:),
	CMD_CONST(CmdMakeTextWritingDirectionLeftToRight, makeTextWritingDirectionLeftToRight:),
	CMD_CONST(CmdMakeTextWritingDirectionRightToLeft, makeTextWritingDirectionRightToLeft:),

	// Not part of NSResponder, something custom we need for Windows
	CMD_CONST(CmdToggleOverwriteMode, rbToggleOverwriteMode),
	CMD_CONST(CmdCopy, rbCopy),
	CMD_CONST(CmdCut, rbCut),
	CMD_CONST(CmdPaste, rbPaste),
	CMD_CONST(CmdUndo, rbUndo)
};



static REALcontrolBehaviour Behavior = 
{
	Initializer,
	nullptr, // destructorFunction
	FirePaintStub,
	MouseDown,
	MouseDrag,
	MouseUp,
	GotFocus,
	LostFocus,
	KeyDownEvent,
	OpenEvent,
	CloseEvent,
	nullptr, // backgroundIdleFunction
	nullptr, // drawOffscreenFunction
	nullptr, // setSpecialBackground
	nullptr, // constantChanging
	nullptr, // droppedNewInstance
	nullptr, // mouseEnterFunction
	nullptr, // mouseExitFunction
	nullptr, // mouseMoveFunction
	nullptr, // stateChangedFunction
	nullptr, // actionEventFunction
	HandleGetter,
	nullptr, // mouseWheelFunction
	EnableMenuItems,
	nullptr, // menuItemActionFunction
	nullptr, // controlAcceptFocus
	nullptr, // keyUpFunction,
	FirePaint,
#if WIDGET_GTK
	UnfilteredKeyDownEvent,
#else
    nullptr,
#endif
    ScaleFactorChanged
};

static REALcontrolBehaviour DesktopBehavior =
{
	DesktopInitializer,
	nullptr, // destructorFunction
	FirePaintStub,
	MouseDown,
	MouseDrag,
	MouseUp,
	GotFocus,
	LostFocus,
	KeyDownEvent,
	OpenEvent,
	CloseEvent,
	nullptr, // backgroundIdleFunction
	nullptr, // drawOffscreenFunction
	nullptr, // setSpecialBackground
	nullptr, // constantChanging
	nullptr, // droppedNewInstance
	nullptr, // mouseEnterFunction
	nullptr, // mouseExitFunction
	nullptr, // mouseMoveFunction
	nullptr, // stateChangedFunction
	nullptr, // actionEventFunction
	HandleGetter,
	nullptr, // mouseWheelFunction
	EnableMenuItems,
	nullptr, // menuItemActionFunction
	nullptr, // controlAcceptFocus
	nullptr, // keyUpFunction,
	FirePaint,
#if WIDGET_GTK
	UnfilteredKeyDownEvent,
#else
    nullptr,
#endif
    ScaleFactorChanged
};

static REALproperty Properties[] =
{
    {"Appearance","AllowFocusRing","Boolean",REALconsoleSafe,(REALproc)GetAllowFocusRing,(REALproc)SetAllowFocusRing},
};



REALcontrol InputCanvasControl =
{
	kCurrentREALControlVersion,
	"TextInputCanvas",
	sizeof(InputCanvasData),
	REALcontrolAcceptFocus | REALcontrolHandlesKeyboardNavigation | REALdontEraseBackground, // flags
	128, 128,
	100, 100, // default width/height
    Properties, 1, // no properties
	Methods, sizeof(Methods) / sizeof(Methods[0]),
	Events, sizeof(Events) / sizeof(Events[0]),
	&Behavior,
	0, // forSystemUse
	nullptr, 0, // event instances
	nullptr, // interfaces
	nullptr, 0, // attributes
	sInputCanvasConstants, sizeof(sInputCanvasConstants) / sizeof(sInputCanvasConstants[0])
};

REALcontrol DesktopInputCanvasControl =
{
	kCurrentREALControlVersion,
	"DesktopTextInputCanvas",
	sizeof(InputCanvasData),
	REALcontrolAcceptFocus | REALcontrolHandlesKeyboardNavigation | REALdontEraseBackground | REALdesktopControl, // flags
	128, 128,
	100, 100, // default width/height
    Properties, 1, // no properties
    Methods, sizeof(Methods) / sizeof(Methods[0]),
	DesktopEvents, sizeof(DesktopEvents) / sizeof(DesktopEvents[0]),
	&DesktopBehavior,
	0, // forSystemUse
	nullptr, 0, // event instances
	nullptr, // interfaces
	nullptr, 0, // attributes
	sInputCanvasConstants, sizeof(sInputCanvasConstants) / sizeof(sInputCanvasConstants[0])
};

void RegisterTextInputCanvasControl()
{
	REALRegisterControl( &InputCanvasControl );

	#ifdef API2
		if (g_XojoVersion >= 2021.03 || g_XojoVersion == 100)
		{
			REALRegisterControl(&DesktopInputCanvasControl);
		}
	#endif

    RectClassRef = REALGetClassRef( RectClassName );
}

// MARK: - TextInputCanvas event forwarding






// FireDoCommand
// 
//  Triggers the user's DoCommand event. This is used for sending arbitrary
//  higher-level commands to the control like "move to the beginning of the
//  document". The list of expected commands are exposed as class constants
//  beginning with Cmd (e.g. CmdMoveWordForward). 
// 
// Gets: control - the control
//            me - handle to the data of the control
//       command - the command to perform
// Returns: whether or not the implementor handled the command
RBBoolean FireDoCommand( REALcontrolInstance control, const char *command )
{
	ControlData(InputCanvasControl, control, InputCanvasData, me);

	if (me->DoCommandEvent)
    {
		REALstring commandStr = REALBuildString( command, (int)strlen( command ), kREALTextEncodingUTF8 );
		RBBoolean result = me->DoCommandEvent( control, commandStr );
		REALUnlockString( commandStr );
		return result;
	}
	
	return false;
}

// FireFontNameAtLocation
// 
//  Triggers the user's FontNameAtLocation event. The implementor should return
//  the font used for rendering the specified character.
// 
// Gets: control - the control
//       location - the zero-based character index
// Returns: the font name
REALstring FireFontNameAtLocation( REALcontrolInstance control, RBInteger location )
{
	InputCanvasData* me = (InputCanvasData*)REALGetControlData(control, &InputCanvasControl);

	return (me->FontNameAtLocationEvent) ? me->FontNameAtLocationEvent(control, location) : NULL;
}

// FireFontSizeAtLocation
// 
//  Triggers the user's FontNameAtLocation event. The implementor should return
//  the font size used for rendering the specified character.
// 
// Gets: control - the control
//       location - the zero-based character index
// Returns: the font size
float FireFontSizeAtLocation( REALcontrolInstance control, RBInteger location )
{
	InputCanvasData* me = (InputCanvasData*)REALGetControlData(control, &InputCanvasControl);

	return (me->FontSizeAtLocationEvent) ? me->FontSizeAtLocationEvent(control, location) : 0.0f;
}

// FireIncompleteTextRange
// 
//  Triggers the user's FireIncompleteTextRange event. The implementor should return
//  the range of the current incomplete text, or nil if there is no incomplete text.
// 
// Gets: control - the control
// Returns: the range of incomplete text (as a TextRange)
REALobject FireIncompleteTextRange( REALcontrolInstance control )
{
	InputCanvasData* me = (InputCanvasData*)REALGetControlData(control, &InputCanvasControl);

	return (me->FireIncompleteTextRangeEvent) ? me->FireIncompleteTextRangeEvent(control) : NULL;
}

// FireInsertSetIncompleteText
// 
//  Triggers the user's SetIncompleteText event. This is fired when the system
//  has started (or is continuing) international text input and wishes to display
//  'incomplete text'. Incomplete text (marked text, in Cocoa terms) is a temporary
//  string displayed in the text during composition of the final input and is
//  not actually part of the content until it has been committed (via InsertText).
//  
// Gets: control - the control
//       text - the marked text (replaces any previous marked text)
//       replacementRange - the range of text to replace
//       relativeSelection - the new selection, relative to the start of the marked
//                           text
// Returns: nothing
void FireSetIncompleteText( REALcontrolInstance control, REALstring text, REALobject replacementRange, REALobject relativeSelection )
{
	InputCanvasData* me = (InputCanvasData*)REALGetControlData(control, &InputCanvasControl);

	if (me->SetIncompleteTextEvent)
	{
		me->SetIncompleteTextEvent(control, text, replacementRange, relativeSelection);
	}
}

// FireInsertText
// 
//  Triggers the user's InsertText event. The implementor should replace the content
//  in its document at `range` with the given text. If `range` is nil, the 
//  implementor should insert the text at its current selection.
// 
// Gets: control - the control
//       text - the text to insert
//       range - the content range to replace (may be NULL)
// Returns: nothing
void FireInsertText( REALcontrolInstance control, REALstring text, REALobject range )
{
	InputCanvasData* me = (InputCanvasData*)REALGetControlData(control, &InputCanvasControl);

	if (me->InsertTextEvent)
	{
		me->InsertTextEvent(control, text, range);
	}
}

// FireIsEditable
// 
//  Triggers the user's IsEditable event. The implementor should return whether
//  or not its content can be edited.
// 
// Gets: control - the control
// Returns: whether or not the content is editable
bool FireIsEditable( REALcontrolInstance control )
{
	InputCanvasData* me = (InputCanvasData*)REALGetControlData(control, &InputCanvasControl);

	return me->IsEditableEvent ? (bool)me->IsEditableEvent(control) : false;
}

bool FireKeyFallsThrough( REALcontrolInstance control, REALstring key )
{
	InputCanvasData* me = (InputCanvasData*)REALGetControlData(control, &InputCanvasControl);

	return me->KeyFallsThroughEvent ? (bool)me->KeyFallsThroughEvent(control, key) : false;
}



#if COCOA
static RBBoolean SimulatedISDarkMode()
{
    return false;
}

RBBoolean IsDarkMode()
{
    static RBBoolean(*IsDarkMode_fp)() = NULL;

    if (IsDarkMode_fp == NULL)
    {
        IsDarkMode_fp = (RBBoolean(*)())REALLoadFrameworkMethod("IsDarkMode() as Boolean");

        if (IsDarkMode_fp == NULL)
        {
            IsDarkMode_fp = &SimulatedISDarkMode;
        }
    }

    return IsDarkMode_fp();
}
#endif

// FirePaint
//  
//  Triggers the user's Paint event.
// 
// Gets: control - the control
//       context - the Graphcis object
// Returns: nothing
void FirePaint( REALcontrolInstance control, REALgraphics context, const Rect *rects, int rectCount )
{
	if (!REALinRuntime())
    {
		Rect bounds;
		REALGetControlBounds(control, &bounds);
        
        if(!GraphicsFunctionsLoaded)
        {
            LoadGraphicsFunctions(context, g_XojoVersion);
        }

        #if COCOA
            REALSetPropValueColor(context, DrawingColor, IsDarkMode() ? 0x00787878 : 0x00B5B5B5);
        #else
            REALSetPropValueColor(context, DrawingColor, 0x00B5B5B5);
        #endif
        
        GraphicsDrawRectangle(context, 0, 0, bounds.right - bounds.left, bounds.bottom - bounds.top);
		return;
	}
    
    ControlData(InputCanvasControl, control, InputCanvasData, me);

	if (me->PaintEvent)
    {
		REALarray rectArray = REALCreateArray( kTypeObject, rectCount - 1 );
		for (int i = 0; i < rectCount; i++) 
		{
			REALobject rectObject = CreateRectObject(RectClassRef, &(rects[i]) );
			REALSetArrayValueObject( rectArray, i, rectObject );
			REALUnlockObject( rectObject );
		}
		
        me->PaintEvent( control, context, rectArray );
		REALUnlockObject( (REALobject)rectArray );
	}
}

// FireRectForRange
// 
//  Triggers the user's RectForRange event. The implementor should return the
//  rectangle occupied by the given range, relative to the control. If needed,
//  the implementor can adjust the range to represent the text that was actually
//  represented in the range (to account for word wrapping or other client-side
//  features).
// 
// Gets: control - the control
//       range - the requested text range
// Returns: the rect the text takes in the control
REALobject FireRectForRange( REALcontrolInstance control, REALobject *range )
{
	InputCanvasData* me = (InputCanvasData*)REALGetControlData(control, &InputCanvasControl);

	return me->RectForRangeEvent ? me->RectForRangeEvent(control, range) : nullptr;
}

// FireSelectedRange
// 
//  Triggers the user's SelectedRange event. The implementor should return a valid
//  range specifying which portion of the content is selected.
// 
// Gets: control - the control
// Returns: the range of the selection
REALobject FireSelectedRange( REALcontrolInstance control )
{
	InputCanvasData* me = (InputCanvasData*)REALGetControlData(control, &InputCanvasControl);

	return me->SelectedRangeEvent ? me->SelectedRangeEvent(control) : nullptr;
}

// FireTextForRange
// 
//  Triggers the user's TextForRange event. The implementor should return the substring
//  of its content at the given range.
// 
// Gets: control - the control
//       range - the range of text to return
// Returns: the substring of the content
REALstring FireTextForRange( REALcontrolInstance control, REALobject range )
{
	InputCanvasData* me = (InputCanvasData*)REALGetControlData(control, &InputCanvasControl);

	return me->TextForRangeEvent ? me->TextForRangeEvent(control, range) : nullptr;
}

// FireTextLength
// 
//  Triggers the user's TextLength event. The implementor should return the length
//  of its content (in characters).
//
// Gets: control - the control
// Returns: the content length
RBInteger FireTextLength( REALcontrolInstance control )
{
	InputCanvasData* me = (InputCanvasData*)REALGetControlData(control, &InputCanvasControl);

	if (me->TextLengthEvent)
	{
		return me->TextLengthEvent(control);
	}

	return 0;
}

// MARK: -

// Initializer
// 
//  The control needs to be created. Do so.
// 
// Gets: control - the TextInputCanvas
// Returns: nothing
static void Initializer( REALcontrolInstance control )
{
	// We need to do this since else sharing the structures is not safe. Xojo never sets for System use on a type that is never used,
	// which is why ClassData can fail when sharing the structs if this is not done.
	if (DesktopInputCanvasControl.forSystemUse == 0)
	{
		DesktopInputCanvasControl.forSystemUse = InputCanvasControl.forSystemUse;
	}
	else if (InputCanvasControl.forSystemUse == 0)
	{
		InputCanvasControl.forSystemUse = DesktopInputCanvasControl.forSystemUse;
	}

	InputCanvasData	*me = (InputCanvasData *)REALGetControlData( control, &InputCanvasControl );

	me->IsDesktopControl = false;

	#if COCOA
		if (REALinRuntime()) {
			me->view = [[XOJTextInputView alloc] initWithControlInstance:control];
		}
	#elif WINDOWS
		me->view = new TextInputWindows( control );
	#elif WIDGET_GTK
		me->view = new TextInputGTK( control );
	#endif
}

static void DesktopInitializer(REALcontrolInstance control)
{
	Initializer(control);

	InputCanvasData* me = (InputCanvasData*)REALGetControlData(control, &InputCanvasControl);

	me->IsDesktopControl = true;
}

// CloseEvent
// 
//  The control is going away, clean up anything we need to.
// 
// Gets: control - the TextInputCanvas
// Returns: nothing
static void CloseEvent( REALcontrolInstance control )
{
	InputCanvasData	*me = (InputCanvasData *)REALGetControlData( control, &InputCanvasControl );
#if COCOA
	[me->view cleanup];
	[me->view release];
	me->view = nil;
#elif WINDOWS || WIDGET_GTK
	delete me->view;
	me->view = nil;
#endif
}

static void PreLoadEvents(REALcontrolInstance control)
{
	InputCanvasData* me = (InputCanvasData*)REALGetControlData(control, &InputCanvasControl);

	REALevent* events = me->IsDesktopControl ? DesktopEvents : Events;

	me->BaselineAtIndexEvent         = (RBInteger(*)(REALcontrolInstance, RBInteger)) REALGetEventInstance(control, &events[0]);
	me->CharacterAtPointEvent        = (RBInteger(*)(REALcontrolInstance, RBInteger, RBInteger))REALGetEventInstance(control, &events[1]);
	me->DiscardIncompleteTextEvent   = (void (*)(REALcontrolInstance))REALGetEventInstance(control, &events[2]);
	me->DoCommandEvent               = (RBBoolean(*)(REALcontrolInstance, REALstring)) REALGetEventInstance(control, &events[3]);
	me->FontNameAtLocationEvent		 = (REALstring(*)(REALcontrolInstance, RBInteger))REALGetEventInstance(control, &events[4]);
	me->FontSizeAtLocationEvent      = (float(*)(REALcontrolInstance, RBInteger))REALGetEventInstance(control, &events[5]);
	me->FireIncompleteTextRangeEvent = (REALobject(*)(REALcontrolInstance))REALGetEventInstance(control, &events[6]);
	me->SetIncompleteTextEvent		 = (void  (*)(REALcontrolInstance, REALstring, REALobject, REALobject))REALGetEventInstance(control, &events[7]);
	me->InsertTextEvent				 = (void  (*)(REALcontrolInstance, REALstring, REALobject))REALGetEventInstance(control, &events[8]);
	me->IsEditableEvent				 = (RBBoolean(*)(REALcontrolInstance))REALGetEventInstance(control, &events[9]);
	me->KeyFallsThroughEvent		 = (RBBoolean(*)(REALcontrolInstance, REALstring))REALGetEventInstance(control, &events[10]);
	me->PaintEvent                   = (void  (*)(REALcontrolInstance, REALgraphics, REALarray))REALGetEventInstance(control, &events[11]);
	me->RectForRangeEvent			 = (REALobject(*)(REALcontrolInstance, REALobject*))REALGetEventInstance(control, &events[12]);
	me->SelectedRangeEvent			 = (REALobject(*)(REALcontrolInstance))REALGetEventInstance(control, &events[13]);
	me->TextForRangeEvent			 = (REALstring(*)(REALcontrolInstance, REALobject))REALGetEventInstance(control, &events[14]);
	me->TextLengthEvent				 = (RBInteger(*)(REALcontrolInstance))REALGetEventInstance(control, &events[15]);
	me->MouseDownEvent				 = (RBBoolean(*)(REALcontrolInstance, RBInteger, RBInteger))REALGetEventInstance(control, &events[16]);
	me->MouseDragEvent				 = (void (*)(REALcontrolInstance, RBInteger, RBInteger))REALGetEventInstance(control, &events[17]);
	me->MouseUpEvent				 = (void (*)(REALcontrolInstance, RBInteger, RBInteger))REALGetEventInstance(control, &events[18]);
	me->GotFocusEvent				 = (void (*)(REALcontrolInstance))REALGetEventInstance(control, &events[19]);
	me->LostFocusEvent				 = (void (*)(REALcontrolInstance))REALGetEventInstance(control, &events[20]);
	me->EnableMenuItemsEvent	     = (void (*)(REALcontrolInstance))REALGetEventInstance(control, &events[21]);
    me->ScaleFactorChangedEvent      = (void (*)(REALcontrolInstance, double)) REALGetEventInstance(control, &events[22]);
}

// OpenEvent
// 
//  The control is has been setup (i.e. at this point the SubPane is created)
// 
// Gets: control - the TextInputCanvas
// Returns: nothing
static void OpenEvent( REALcontrolInstance control )
{
	PreLoadEvents(control);
    
    #if WINDOWS || WIDGET_GTK
        InputCanvasData *me = (InputCanvasData *)REALGetControlData( control, &InputCanvasControl );
    
        me->view->Setup();
    #endif
}

#if !TARGET_MACOS
// InterpretKeyEvent
// 
//  Given a keyboard event, interprets them into higher level commands like "move
//  insertion point up" and sends them to the control.
// 
//  This is not used on Cocoa, where we just let NSView tell us what to do.
// 
// Gets: keycode - the input keycode
//       modifiers - the keyboard modifiers (alt, control, etc)
// Returns: whether or not the event was handled
static RBBoolean InterpretKeyEvent( REALcontrolInstance control, int keycode, int modifiers )
{
	switch (keycode) {
		case 1 /* home */: {
			if (modifiers & controlKey) {
				if (modifiers & shiftKey) {
					return FireDoCommand( control, "moveToBeginningOfDocumentAndModifySelection:" );
				}
				return FireDoCommand( control, "moveToBeginningOfDocument:" );
			} else {
				if (modifiers & shiftKey) {
					return FireDoCommand( control, "moveToLeftEndOfLineAndModifySelection:" );
				}
				return FireDoCommand( control, "moveToLeftEndOfLine:" );
			}
		} break;
			
		case 4 /* end */: {
			if (modifiers & controlKey) {
				if (modifiers & shiftKey) {
					return FireDoCommand( control, "moveToEndOfDocumentAndModifySelection:" );
				}
				return FireDoCommand( control, "moveToEndOfDocument:" );
			} else {
				if (modifiers & shiftKey) {
					return FireDoCommand( control, "moveToRightEndOfLineAndModifySelection:" );
				}
				return FireDoCommand( control, "moveToRightEndOfLine:" );
			}
		} break;
			
		case 11 /* page up */: {
			if (modifiers & shiftKey) {
				return FireDoCommand( control, "pageUpAndModifySelection:" );
			}
			return FireDoCommand( control, "pageUp:" );
		} break;
			
		case 12 /* page down */: {
			if (modifiers & shiftKey) {
				return FireDoCommand( control, "pageDownAndModifySelection:" );
			}
			return FireDoCommand( control, "pageDown:" );
		} break;
			
		case 8 /* backspace */: {
#if WINDOWS
			if (modifiers & optionKey) {
				return FireDoCommand( control, "rbUndo" );
			}
#endif
			
			return FireDoCommand( control, "deleteBackward:" );
		} break;
			
		case 127 /* forward delete */: {
#if WINDOWS
			if (modifiers & shiftKey) {
				return FireDoCommand( control, "rbCut" );
			}
#endif	
			
			return FireDoCommand( control, "deleteForward:" );
		} break;
			
		case 5 /* insert */: {
#if WINDOWS
			if (modifiers & shiftKey) {
				return FireDoCommand( control, "rbPaste" );
			} else if (modifiers & controlKey) {
				return FireDoCommand( control, "rbCopy" );
			} else {
				return FireDoCommand( control, "rbToggleOverwriteMode" );
			}
#endif
		} break;

		case 13 /* return */:
		case 3  /* enter */:
		{
			// On Windows Ctrl+C also triggers this keycode (3), so make sure the controlKey isn't down
			if (!(modifiers & controlKey)) {
				return FireDoCommand( control, "insertNewline:" );
			}
		} break;

		case 28 /* left arrow */: {
			if ((modifiers & shiftKey) && (modifiers & controlKey)) {
				return FireDoCommand( control, "moveWordLeftAndModifySelection:" );
			} else if (modifiers & shiftKey) {
				return FireDoCommand( control, "moveLeftAndModifySelection:" );
			} else if (modifiers & controlKey) {
				return FireDoCommand( control, "moveWordLeft:" );
			} else {
				return FireDoCommand( control, "moveLeft:" );
			}
		} break;
			
		case 29 /* right arrow */: {
			if ((modifiers & shiftKey) && (modifiers & controlKey)) {
				return FireDoCommand( control, "moveWordRightAndModifySelection:" );
			} else if (modifiers & shiftKey) {
				return FireDoCommand( control, "moveRightAndModifySelection:" );
			} else if (modifiers & controlKey) {
				return FireDoCommand( control, "moveWordRight:" );
			} else {
				return FireDoCommand( control, "moveRight:" );
			}
		} break;
			
		case 30 /* up arrow */: {
			if (modifiers & shiftKey) {
				return FireDoCommand( control, "moveUpAndModifySelection:" );
			} else if (modifiers & controlKey) {
				return FireDoCommand( control, "scrollLineUp:" );
			} else {
				return FireDoCommand( control, "moveUp:" );
			}
		} break;
			
		case 31 /* down arrow */: {
			if (modifiers & shiftKey) {
				return FireDoCommand( control, "moveDownAndModifySelection:" );
			} else if (modifiers & controlKey) {
				return FireDoCommand( control, "scrollLineDown:" );
			} else {
				return FireDoCommand( control, "moveDown:" );
			}
		} break;
			
		case 9 /* tab */: {
			if (modifiers & shiftKey) {
				return FireDoCommand( control, "insertBacktab:" );
			} else {
				return FireDoCommand( control, "insertTab:" );
			}
		} break;
	}

	// Keys are translated in a separate event, returning true here just prevents
	// our framework from dispatching this further (i.e. to ContainerControls, and Window)
	return true;
}
#endif  // !TARGET_MACOS

#if WIDGET_GTK
// IsTextKey
// 
//  Return whether the given key is a bit of text input, as opposed to a menu
//  shortcut or some such. This generally involves checking the current keyboard
//  modifiers, but the details vary from platform to platform.
// 
//  This is not used on Cocoa, where we just let NSView tell us what to do.
// 
// Gets: keycode - the keycode to check
//       modifiers - the keyboard modifiers (alt, control, etc)
// Returns: is this a text key
static bool IsTextKey( int keycode, int modifiers )
{
	// Rule 1: if it's some control character (ASCII 0-31), then it's not text.
	// IMPORTANT: don't exclude other non-ASCII characters here!  Some people find
	// those characters pretty important.
	if (keycode < 32) return false;

	// Windows/Linux rule: if the control key is down without the alt key, or
	// vice versa, then it's a shortcut and not keyboard input. (Note: we must be
	// sure to allow AltGr combinations, i.e. Ctrl+Alt+SomeKey.)
	if ((modifiers & controlKey) && !(modifiers & optionKey)) return false;
	if ((modifiers & optionKey) && !(modifiers & controlKey)) return false;

	return true;
}
#endif  // WIDGET_GTK

static RBBoolean KeyDownEvent( REALcontrolInstance control, int charCode, int keyCode, int modifiers )
{
	#if !TARGET_MACOS
		return InterpretKeyEvent( control, charCode, modifiers );
	#else
		return false;
	#endif
}

#if WIDGET_GTK
static RBBoolean UnfilteredKeyDownEvent( REALcontrolInstance control, int keyCode, int modifiers )
{
	InputCanvasData	*data = (InputCanvasData *)REALGetControlData( control, &InputCanvasControl );
	data->view->KeyDown();
	return false;
}
#endif

// InvalidateTextRects
// 
//  Invalidates any cached text rectangles. The control should call this whenever
//  content moves around on screen.
// 
// Gets: control - the control
// Returns: nothing
static void InvalidateTextRects( REALcontrolInstance control )
{
	InputCanvasData	*data = (InputCanvasData *)REALGetControlData( control, &InputCanvasControl );
#if COCOA
	[[data->view inputContext] invalidateCharacterCoordinates];
#endif
}

static void LinuxSelectInputMethod( REALcontrolInstance control )
{
#if WIDGET_GTK
	InputCanvasData	*data = (InputCanvasData *)REALGetControlData( control, &InputCanvasControl );
	data->view->SelectInputMethod();
#endif
}

static RBBoolean MouseDown( REALcontrolInstance control, int x, int y, int modifiers )
{
	InputCanvasData* me = (InputCanvasData*)REALGetControlData(control, &InputCanvasControl);

	if (me->MouseDownEvent)
	{
		Rect bounds;
		REALGetControlBounds(control, &bounds);

		return me->MouseDownEvent(control, x - bounds.left, y - bounds.top);
	}

	return false;
}

static void MouseDrag( REALcontrolInstance control, int x, int y )
{
	InputCanvasData* me = (InputCanvasData*)REALGetControlData(control, &InputCanvasControl);

	if (me->MouseDragEvent)
	{
		Rect bounds;
		REALGetControlBounds(control, &bounds);

		me->MouseDragEvent(control, x - bounds.left, y - bounds.top);
	}
}

static void MouseUp( REALcontrolInstance control, int x, int y )
{
	InputCanvasData* me = (InputCanvasData*)REALGetControlData(control, &InputCanvasControl);

	if (me->MouseUpEvent)
	{
		Rect bounds;
		REALGetControlBounds(control, &bounds);

		me->MouseUpEvent(control, x - bounds.left, y - bounds.top);
	}
}

static void ScaleFactorChanged(REALcontrolInstance control, double newScaleFactor)
{
    InputCanvasData* me = (InputCanvasData*)REALGetControlData(control, &InputCanvasControl);
    
    if(me->ScaleFactorChangedEvent)
    {
        me->ScaleFactorChangedEvent(control, newScaleFactor);
    }
}


static void GotFocus( REALcontrolInstance control )
{
	InputCanvasData* me = (InputCanvasData*)REALGetControlData(control, &InputCanvasControl);

	#if WIDGET_GTK
		me->view->GotFocus();
	#endif
    
    me->HasFocus = true;

	if (me->GotFocusEvent)
	{
		me->GotFocusEvent(control);
	}
}

static void LostFocus( REALcontrolInstance control )
{
	InputCanvasData* me = (InputCanvasData*)REALGetControlData(control, &InputCanvasControl);

	#if WIDGET_GTK
		me->view->LostFocus();
	#endif
    
    me->HasFocus = false;

	if (me->LostFocusEvent)
	{
		me->LostFocusEvent(control);
	}
	
}
static void EnableMenuItems( REALcontrolInstance control )
{
	InputCanvasData* me = (InputCanvasData*)REALGetControlData(control, &InputCanvasControl);

	if (me->EnableMenuItemsEvent)
	{
		me->EnableMenuItemsEvent(control);
	}
}

static void *HandleGetter( REALcontrolInstance control )
{
	InputCanvasData	*data = (InputCanvasData *)REALGetControlData( control, &InputCanvasControl );
    #if COCOA
        return data->view;
    #else
        return NULL;
    #endif
}

