//
//  main.cpp
//
//  (c) 2013 Xojo, Inc. -- All Rights Reserved
//

#include "TextInputCanvas.h"
#include "TextRange.h"
#include "XojoAPISwitch.h"

double g_XojoVersion;

void PluginEntry()
{
    g_XojoVersion = REALGetRBVersion();
    
    InitApiSwitch();
    
	RegisterTextRangeClass();
	RegisterTextInputCanvasControl();
}
