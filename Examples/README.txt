Most complete usage for this is of course the FormattedTextControl at:
https://github.com/atomiccity/FormattedTextControl


We think the TIC Example project was provided by Xojo at the time 
When the TextInputCanvas first came. If this is not true and someone has
ownership and claim to the TIC Example project and does not want it here
then we are more than willing to remove it. Or if someone has more 
Correct story of its origin then by all means let us know so this can 
Be updated.

